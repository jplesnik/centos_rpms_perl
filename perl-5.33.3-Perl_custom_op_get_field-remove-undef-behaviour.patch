From e050064b67c501e9fdc7bc3f513ba2b8b9e795f8 Mon Sep 17 00:00:00 2001
From: David Mitchell <davem@iabyn.com>
Date: Fri, 30 Oct 2020 20:50:58 +0000
Subject: [PATCH] Perl_custom_op_get_field(): remove undef behaviour
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Thus function has a couple a switches with

               default:
                   NOT_REACHED; /* NOTREACHED */

but clang is complaining that the value returned by the function is
undefined if those default branches are taken, since the 'any' variable
doesn't get set in that path.

Replace the NOTREACHED with a croak("panic: ..."). It's possible (albeit
not intended) for Perl_custom_op_get_field() to be called with a 'field'
arg which triggers the default case. So if this ever happens, make it
clear that something has gone wrong, rather than just silently
continuing on non-debugging builds.

In any case, this shuts up clang.

Signed-off-by: Petr Písař <ppisar@redhat.com>
---
 op.c | 14 ++++++--------
 1 file changed, 6 insertions(+), 8 deletions(-)

diff --git a/op.c b/op.c
index c30c6b7c8f..2933e2ed7d 100644
--- a/op.c
+++ b/op.c
@@ -18100,6 +18100,7 @@ Perl_custom_op_get_field(pTHX_ const OP *o, const xop_flags_enum field)
 	else
 	    xop = INT2PTR(XOP *, SvIV(HeVAL(he)));
     }
+
     {
 	XOPRETANY any;
 	if(field == XOPe_xop_ptr) {
@@ -18121,7 +18122,10 @@ Perl_custom_op_get_field(pTHX_ const OP *o, const xop_flags_enum field)
 		    any.xop_peep = xop->xop_peep;
 		    break;
 		default:
-		    NOT_REACHED; /* NOTREACHED */
+                  field_panic:
+                    Perl_croak(aTHX_
+                        "panic: custom_op_get_field(): invalid field %d\n",
+                        (int)field);
 		    break;
 		}
 	    } else {
@@ -18139,17 +18143,11 @@ Perl_custom_op_get_field(pTHX_ const OP *o, const xop_flags_enum field)
 		    any.xop_peep = XOPd_xop_peep;
 		    break;
 		default:
-		    NOT_REACHED; /* NOTREACHED */
+                    goto field_panic;
 		    break;
 		}
 	    }
 	}
-        /* On some platforms (HP-UX, IA64) gcc emits a warning for this function:
-         * op.c: In function 'Perl_custom_op_get_field':
-         * op.c:...: warning: 'any.xop_name' may be used uninitialized in this function [-Wmaybe-uninitialized]
-         * This is because on those platforms (with -DEBUGGING) NOT_REACHED
-         * expands to assert(0), which expands to ((0) ? (void)0 :
-         * __assert(...)), and gcc doesn't know that __assert can never return. */
 	return any;
     }
 }
-- 
2.25.4

